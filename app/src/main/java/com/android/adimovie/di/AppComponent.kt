package com.android.adimovie.di

import com.android.adimovie.core.di.coreModule
import com.android.adimovie.features.home.di.featureHome

val appComponent = listOf(
    coreModule,
    featureHome
)