object Modules {
    const val core = ":Core"
    const val common = ":Common"
    const val featureHome = ":features:home"
    const val featureSplash = ":features:splash"
    const val UiResources = ":UiResources"
    const val Navigation = ":Navigation"
}