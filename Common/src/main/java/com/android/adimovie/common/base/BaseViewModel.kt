package com.android.adimovie.common.base

import androidx.lifecycle.*
import com.github.ajalt.timberkt.i
import org.koin.core.component.KoinComponent

abstract class BaseViewModel : ViewModel(), KoinComponent {

    override fun onCleared() {
        super.onCleared()
        i { "ViewModel Cleared" }
    }
}