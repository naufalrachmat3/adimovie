package com.android.adimovie.common.base

import android.app.Application
import android.content.Context
import com.android.adimovie.core.BuildConfig
import com.github.ajalt.timberkt.e
import com.github.ajalt.timberkt.w
import io.reactivex.rxjava3.exceptions.UndeliverableException
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import java.io.IOException
import java.net.SocketException

abstract class BaseApp : Application() {

    fun context(): Context = applicationContext

    companion object {
        lateinit var instance: BaseApp
    }

    override fun onCreate() {
        super.onCreate()

        instance = this
        //addRxJavaExceptionHandler()
    }
}