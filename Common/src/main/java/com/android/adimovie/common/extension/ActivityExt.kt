package com.android.adimovie.common.extension

import android.app.Activity

inline fun <T> Activity.extra(key: String): Lazy<T> {
    return lazy(LazyThreadSafetyMode.NONE) {
        @Suppress("UNCHECKED_CAST")
        intent?.extras?.get(key) as T
    }
}

inline fun <T> Activity.extraOrNull(key: String): Lazy<T?> {
    return lazy(LazyThreadSafetyMode.NONE) {
        @Suppress("UNCHECKED_CAST")
        intent?.extras?.get(key) as? T?
    }
}