package com.android.adimovie.navigation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.annotation.AnimRes
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat

private const val PACKAGE_NAME = "com.android.adimovie"
private const val FEATURES = "com.android.adimovie"

/**
 * Create an Intent with [Intent.ACTION_VIEW] to an [AddressableActivity].
 */
fun Context.intentTo(addressableActivity: AddressableActivity): Intent {
    return Intent(Intent.ACTION_VIEW)
        .setClassName(this, addressableActivity.className)
}
fun Context.startFeature(
    addressableActivity: AddressableActivity,
    @AnimRes enterResId: Int = android.R.anim.fade_in,
    @AnimRes exitResId: Int = android.R.anim.fade_out,
    options: Bundle? = null,
    body: Intent.() -> Unit) {

    val intent = intentTo(addressableActivity)
    intent.body()

    if (options == null) {
        val optionsCompat = ActivityOptionsCompat.makeCustomAnimation(this, enterResId, exitResId)
        ActivityCompat.startActivity(this, intent, optionsCompat.toBundle())
    } else {
        ActivityCompat.startActivity(this, intent, options)
    }
}


interface AddressableActivity {
    val className: String
}
object Activities {
    object HomeActivity : AddressableActivity {
        override val className = "com.android.adimovie.features.home.HomeActivity"
    }
    object HomeMovieCategoryActivity : AddressableActivity {
        override val className = "com.android.adimovie.features.home.HomeMovieCategoryActivity"
    }
    object HomeMovieActivity : AddressableActivity {
        override val className = "com.android.adimovie.features.home.HomeMovieActivity"
    }
    object HomeMovieActivityDetail : AddressableActivity {
        override val className = "com.android.adimovie.features.home.HomeMovieActivityDetail"
    }
    object HomeMovieActivityTrailer : AddressableActivity {
        override val className = "com.android.adimovie.features.home.HomeMovieActivityTrailer"
    }
    object HomeMovieActivityReview : AddressableActivity {
        override val className = "com.android.adimovie.features.home.HomeMovieActivityReview"
    }
    object HomeMovieActivityVideo : AddressableActivity {
        override val className = "com.android.adimovie.features.home.HomeMovieActivityVideo"
    }
}