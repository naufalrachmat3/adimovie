package com.android.adimovie.core.model

object AppConstant {
    const val Accept = "Accept"
    const val asJson = "application/json"
    const val Authorization = "Authorization"
}