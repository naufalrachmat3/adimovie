package com.android.adimovie.features.splash

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.android.adimovie.navigation.Activities
import com.android.adimovie.navigation.startFeature

/*import com.android.adimovie.navigation.Activities
import com.android.adimovie.navigation.startFeature*/

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed(Runnable {
               run {
                   startFeature(Activities.HomeMovieCategoryActivity){}
                   finish()
               }
        } , 1500)
    }
}