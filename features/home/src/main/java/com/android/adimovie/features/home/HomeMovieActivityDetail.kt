package com.android.adimovie.features.home

import android.os.Bundle
import com.android.adimovie.common.Constants
import com.android.adimovie.common.base.BaseActivity
import com.android.adimovie.common.extension.extra
import com.android.adimovie.common.extension.formatDate
import com.android.adimovie.common.utils.ViewState
import com.android.adimovie.common.view.StateLayout
import com.android.adimovie.features.home.databinding.HomeMovieActivityDetailBinding
import com.android.adimovie.navigation.Activities
import com.android.adimovie.navigation.startFeature
import com.android.adimovie.common.extension.binding
import com.android.adimovie.common.extension.loadImageRounded

class HomeMovieActivityDetail : BaseActivity<HomeViewModel>() {
    private val binding by binding<HomeMovieActivityDetailBinding>()
    private val movieId by extra<String>(Constants.MOVIE_ID)
    private val movieTitle by extra<String>(Constants.MOVIE_TITLE)
    private val stateLayout by lazy {
        StateLayout(this)
            .wrap(binding.constraintData)
            .showLoading()
    }
    override fun getViewModel() = HomeViewModel::class
    override fun observerViewModel() {
        viewModel.movieDetailResponse.onResult { state ->
            when (state) {
                is ViewState.Loading -> stateLayout.showLoading()
                is ViewState.Success -> {
                    stateLayout.showContent()
                    binding.apply {
                        movieImage.loadImageRounded("${BuildConfig.BASE_IMAGE_URL}/${state.data.poster_path}")
                        movieTitleText.text = state.data.title
                        durationText.text = state.data.runtime.toString()
                        statusText.text = state.data.status
                        releaseDateText.text = state.data.release_date.formatDate(destFormat = Constants.DEFAULT_DATE_TIME)
                        avgText.text = state.data.vote_average.toString()
                        avgCountText.text = state.data.vote_count.toString()
                        overviewText.text = state.data.overview
                    }
                }
                is ViewState.Failed -> {
                    stateLayout.showError()
                    stateLayout.onRetry {
                        viewModel.getMovieDetail(movieId)
                    }
                }
            }
        }
    }

    override fun onViewCreated(savedInstanceState: Bundle?) {
        binding.apply {
            titleText.text = movieTitle
            icBack.setOnClickListener {
                onBackPressed()
            }
            btnSeeTrailer.setOnClickListener {
                startFeature(Activities.HomeMovieActivityTrailer){
                    putExtra(Constants.MOVIE_ID , movieId)
                    putExtra(Constants.MOVIE_TITLE , movieTitle)
                }
            }
            btnSeeReview.setOnClickListener {
                startFeature(Activities.HomeMovieActivityReview){
                    putExtra(Constants.MOVIE_ID , movieId)
                    putExtra(Constants.MOVIE_TITLE , movieTitle)
                }
            }
            swipeRefresh.setOnRefreshListener {
                viewModel.getMovieDetail(movieId)
                swipeRefresh.isRefreshing = false
            }
        }
        viewModel.getMovieDetail(movieId)
    }
}