package com.android.adimovie.features.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.android.adimovie.common.base.BaseAdapter
import com.android.adimovie.common.base.BaseViewHolder
import com.android.adimovie.core.BuildConfig
import com.android.adimovie.core.model.video.Video
import com.android.adimovie.features.home.databinding.ItemVideoBinding
import com.android.adimovie.common.extension.loadImageRounded

class VideoAdapter : BaseAdapter<Video , ItemVideoBinding>() {
    private var listener : VideoListener? = null

    fun setVideoListener(listener : VideoListener){
        this.listener = listener
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<Video, ItemVideoBinding> {
        val binding = ItemVideoBinding.inflate(LayoutInflater.from(parent.context) , parent , false)
        return ViewHolder(binding)
    }
    inner class ViewHolder(binding : ItemVideoBinding) : BaseViewHolder<Video , ItemVideoBinding>(binding){
        override fun bind(item: Video, position: Int) {
            binding.apply {
                videoImage.loadImageRounded("${BuildConfig.BASE_YOUTUBE_IMAGE_URL}${item.key}/maxresdefault.jpg")
                videoTitleText.text = item.name
                parentContainer.setOnClickListener {
                    listener?.onClick(item)
                }
            }
        }
    }
    interface VideoListener {
        fun onClick(video : Video)
    }
}