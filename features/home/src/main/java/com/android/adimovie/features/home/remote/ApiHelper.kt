package com.android.adimovie.features.home.remote

import com.android.adimovie.common.utils.ViewState
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

object ApiHelper {

    suspend fun <T : Any> getResult(call: suspend () -> Response<T>): ViewState<T> {
        try {
            val response = call()
            if (response.isSuccessful) {
                val body = response.body()
                body?.let {
                    return ViewState.Success(body)
                } ?: return ViewState.Failed("Response is empty")
            } else {
                val errorCode = response.code()
                val errorMsg = parseErrorMessage(errorCode)
                return ViewState.Failed(errorMsg)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return ViewState.Failed(
                when (e) {
                    is IOException -> "No Internet Connection"
                    is UnknownHostException -> "Bad Gateway"
                    is SocketTimeoutException -> "Request Timeout"
                    is HttpException -> "Bad Request"
                    else -> "Internal Server Error"
                }
            )
        }
    }
    private fun parseErrorMessage(errorCode: Int): String = when (errorCode) {
        502 -> "Bad Gateway"
        504 -> "Gateway Timeout"
        401 -> "Unauthorized"
        408 -> "Request Timeout"
        400 -> "Bad Request"
        404 -> "Response Not Found"
        else -> "Internal Server Error."
    }
}