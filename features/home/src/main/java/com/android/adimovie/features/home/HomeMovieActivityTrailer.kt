package com.android.adimovie.features.home

import android.os.Bundle
import androidx.annotation.NonNull
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.adimovie.common.Constants
import com.android.adimovie.common.base.BaseActivity
import com.android.adimovie.common.extension.extra
import com.android.adimovie.common.utils.ViewState
import com.android.adimovie.common.view.StateLayout
import com.android.adimovie.core.model.video.Video
import com.android.adimovie.features.home.adapter.VideoAdapter
import com.android.adimovie.features.home.databinding.HomeMovieActivityTrailerBinding
import com.android.adimovie.navigation.Activities
import com.android.adimovie.navigation.startFeature
import com.android.adimovie.common.extension.binding
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer

import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener

class HomeMovieActivityTrailer : BaseActivity<HomeViewModel>() {
    private val binding by binding<HomeMovieActivityTrailerBinding>()
    private val movieId by extra<String>(Constants.MOVIE_ID)
    private val movieTitle by extra<String>(Constants.MOVIE_TITLE)
    private val videoAdapter by lazy {
        VideoAdapter()
    }
    private val stateLayout by lazy {
        StateLayout(this)
            .wrap(binding.recyclerViewMovieTrailer)
    }
    override fun getViewModel() = HomeViewModel::class
    override fun observerViewModel() {
        viewModel.videoResponse.onResult { state ->
            when (state) {
                is ViewState.Loading -> stateLayout.showLoading()
                is ViewState.Success -> {
                    if(state.data.results.size > 0){
                        stateLayout.showContent()
                        videoAdapter.submitList(state.data.results)
                    } else {
                        stateLayout.showEmpty(noDataText = "Tidak ada video pada film ini")
                    }
                }
                is ViewState.Failed -> {
                    stateLayout.showError()
                    stateLayout.onRetry {
                        viewModel.getVideos(movieId)
                    }
                }
            }
        }
    }

    override fun onViewCreated(savedInstanceState: Bundle?) {
        binding.apply {
            titleText.text = "Video : $movieTitle"
            icBack.setOnClickListener {
                onBackPressed()
            }
            recyclerViewMovieTrailer.apply {
                layoutManager = LinearLayoutManager(this@HomeMovieActivityTrailer , RecyclerView.VERTICAL , false)
                adapter = videoAdapter
            }
        }
        videoAdapter.setVideoListener(object : VideoAdapter.VideoListener {
            override fun onClick(video: Video) {
                startFeature(Activities.HomeMovieActivityVideo){
                    putExtra(Constants.VIDEO_ID , video.key)
                }
            }
        })
        viewModel.getVideos(movieId)
    }
}