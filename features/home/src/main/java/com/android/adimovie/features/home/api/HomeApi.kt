package com.android.adimovie.features.home.api

import com.android.adimovie.core.model.category.CategoryResponse
import com.android.adimovie.core.model.movie.MovieDetailResponse
import com.android.adimovie.core.model.movie.MovieResponse
import com.android.adimovie.core.model.review.ReviewResponse
import com.android.adimovie.core.model.video.VideoResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface HomeApi {
    @GET("genre/movie/list")
    suspend fun getMovieCategories() : CategoryResponse
    @GET("discover/movie")
    suspend fun getMovieByCategory(
        @Query("page") page : Int ,
        @Query("sort_by") sortBy : String,
        @Query("with_genres") withGenres : String
    ) : Response<MovieResponse>
    @GET("movie/{movie_id}")
    suspend fun getMovieDetail(
        @Path("movie_id") movieId : String
    ) : MovieDetailResponse
    @GET("movie/{movie_id}/reviews")
    suspend fun getReviews(
        @Path("movie_id") movieId : String,
        @Query("page") page : Int
    ) : Response<ReviewResponse>

    @GET("movie/{movie_id}/videos")
    suspend fun getVideos(
        @Path("movie_id") movieId : String
    ) : VideoResponse
}