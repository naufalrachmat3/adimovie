package com.android.adimovie.features.home

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.adimovie.common.Constants
import com.android.adimovie.common.base.BaseActivity
import com.android.adimovie.common.extension.extra
import com.android.adimovie.common.view.StateLayout
import com.android.adimovie.core.model.movie.Movie
import com.android.adimovie.features.home.adapter.MovieAdapter
import com.android.adimovie.features.home.adapter.MovieDiffUtil
import com.android.adimovie.features.home.databinding.HomeMovieActivityBinding
import com.android.adimovie.navigation.Activities
import com.android.adimovie.navigation.startFeature
import com.android.adimovie.common.extension.binding
import com.github.ajalt.timberkt.d
import com.github.ajalt.timberkt.e
import kotlinx.android.synthetic.main.home_movie_activity.*
import kotlinx.coroutines.flow.collectLatest

class HomeMovieActivity : BaseActivity<HomeViewModel>() {
    private val binding by binding<HomeMovieActivityBinding>()
    private val genreTitle by extra<String>(Constants.GENRE_TITLE)
    private val genres by extra<Int>(Constants.GENRES)
    private val movieAdapter by lazy {
        MovieAdapter(MovieDiffUtil())
    }
    private val stateLayout by lazy {
        StateLayout(this)
            .wrap(binding.recyclerViewMovie)
    }
    override fun getViewModel() = HomeViewModel::class
    override fun observerViewModel() {

    }

    override fun onViewCreated(savedInstanceState: Bundle?) {
        binding.apply {
            titleText.text = genreTitle
            recyclerViewMovie.apply {
                layoutManager = GridLayoutManager(this@HomeMovieActivity , 2 , RecyclerView.VERTICAL , false)
                adapter = movieAdapter
            }
            icBack.setOnClickListener {
                onBackPressed()
            }
        }
        movieAdapter.setMovieListener(object : MovieAdapter.MovieListener {
            override fun onClick(movie: Movie) {
                startFeature(Activities.HomeMovieActivityDetail){
                    putExtra(Constants.MOVIE_ID , movie.id.toString())
                    putExtra(Constants.MOVIE_TITLE , movie.title)
                }
            }
        })
        getMovies()
    }
    private fun getMovies(){
        viewModel.getMovieByCategory("popularity.desc" , genres.toString()).observe(this , Observer {
            movieAdapter.submitData(lifecycle , it)
        })
        lifecycleScope.launchWhenCreated { movieAdapter.loadStateFlow.collectLatest { renderMovieResponse(it.refresh) } }
    }
    private fun renderMovieResponse(state : LoadState){
        when(state) {
            is LoadState.Loading -> {
                stateLayout.showLoading()
            }
            is LoadState.NotLoading -> {
                if(movieAdapter.itemCount > 0){
                    stateLayout.showContent()
                } else {
                    stateLayout.showEmpty(noDataText = "Tidak ada film untuk genre ini")
                }
            }
            is LoadState.Error -> {
                stateLayout.showError()
                stateLayout.onRetry {
                    getMovies()
                }
            }
        }
    }
}