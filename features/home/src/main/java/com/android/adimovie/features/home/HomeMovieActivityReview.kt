package com.android.adimovie.features.home

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.adimovie.common.Constants
import com.android.adimovie.common.base.BaseActivity
import com.android.adimovie.common.extension.extra
import com.android.adimovie.common.view.StateLayout
import com.android.adimovie.features.home.adapter.ReviewAdapter
import com.android.adimovie.features.home.adapter.ReviewDiffUtil
import com.android.adimovie.features.home.databinding.HomeMovieReviewActivityBinding
import com.android.adimovie.common.extension.binding
import kotlinx.coroutines.flow.collectLatest

class HomeMovieActivityReview : BaseActivity<HomeViewModel>() {
    private val binding by binding<HomeMovieReviewActivityBinding>()
    private val movieId by extra<String>(Constants.MOVIE_ID)
    private val movieTitle by extra<String>(Constants.MOVIE_TITLE)
    private val reviewAdapter by lazy { ReviewAdapter(ReviewDiffUtil()) }
    private val stateLayout by lazy {
        StateLayout(this)
            .wrap(binding.recyclerViewReview)
    }
    override fun getViewModel() = HomeViewModel::class
    override fun observerViewModel() {

    }

    override fun onViewCreated(savedInstanceState: Bundle?) {
        binding.apply {
            titleText.text = "Review : $movieTitle"
            recyclerViewReview.apply {
                layoutManager = LinearLayoutManager(this@HomeMovieActivityReview , RecyclerView.VERTICAL , false)
                adapter = reviewAdapter
            }
            icBack.setOnClickListener {
                onBackPressed()
            }
        }
        getReviews()
    }
    private fun getReviews(){
        viewModel.getReviews(movieId).observe(this , Observer {
            reviewAdapter.submitData(lifecycle , it)
        })
        lifecycleScope.launchWhenCreated { reviewAdapter.loadStateFlow.collectLatest { renderReviewResponse(it.refresh) } }
    }
    private fun renderReviewResponse(state : LoadState){
        when(state){
            is LoadState.Loading -> {
                stateLayout.showLoading()
            }
            is LoadState.NotLoading -> {
                if(reviewAdapter.itemCount > 0){
                    stateLayout.showContent()
                } else {
                    stateLayout.showEmpty(noDataText = "Belum ada review pada film ini")
                }
            }
            is LoadState.Error -> {
                stateLayout.showError()
                stateLayout.onRetry {
                    getReviews()
                }
            }
        }
    }
}