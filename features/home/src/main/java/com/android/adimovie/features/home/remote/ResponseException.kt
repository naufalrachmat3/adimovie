package com.android.adimovie.features.home.remote

sealed class ResponseException : Exception() {
    data class Error(val errorResponse: ErrorResponse) : ResponseException()
    object Empty : ResponseException()
}