package com.android.adimovie.features.home.remote

import androidx.paging.PagingSource
import com.android.adimovie.common.utils.ViewState
import com.android.adimovie.core.model.review.Review
import com.android.adimovie.features.home.api.HomeApi

class ReviewPagingSource(
    private val api : HomeApi,
    private val movieId : String
) : PagingSource<Int , Review>() {
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Review> {
        return try {
            val page = params.key ?: 1
            val prevKey = if(page == 1) null else page - 1
            when(val response = ApiHelper.getResult { api.getReviews(movieId , page) }){
                is ViewState.Success -> {
                    response.data.results?.let {
                        val resultData = it
                        if(resultData.isNullOrEmpty()){
                            LoadResult.Error(ResponseException.Empty)
                        } else {
                            LoadResult.Page(
                                data = resultData,
                                prevKey = prevKey,
                                nextKey = page.plus(1)
                            )
                        }
                    }
                }
                is ViewState.Failed -> LoadResult.Error(ResponseException.Error(ErrorResponse()))
                else -> LoadResult.Error(ResponseException.Error(ErrorResponse(500 , "Something wrong happened.")))
            }
        } catch (e : Exception){
            LoadResult.Error(e)
        }
    }
}