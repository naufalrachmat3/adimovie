package com.android.adimovie.features.home

import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import com.android.adimovie.common.Constants
import com.android.adimovie.common.base.BaseActivity
import com.android.adimovie.common.extension.extra
import com.android.adimovie.features.home.databinding.HomeMovieActivityVideoBinding
import com.android.adimovie.common.extension.binding
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerFullScreenListener

class HomeMovieActivityVideo : BaseActivity<HomeViewModel>() {
    private val binding by binding<HomeMovieActivityVideoBinding>()
    private val videoId by extra<String>(Constants.VIDEO_ID)
    override fun getViewModel() = HomeViewModel::class
    override fun observerViewModel() {

    }

    override fun onViewCreated(savedInstanceState: Bundle?) {
        binding.apply {
            lifecycle.addObserver(youtubePlayerView)
            youtubePlayerView.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
                override fun onReady(youTubePlayer: YouTubePlayer) {
                    youTubePlayer.loadVideo(videoId , 0f)
                }
            })
            if (requestedOrientation != ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (requestedOrientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }
    }
}