package com.android.adimovie.features.home.remote

import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.android.adimovie.features.home.api.HomeApi

class MovieDataSource(
    private val api : HomeApi
) {
    fun getMovieByCategory(sortBy : String , withGenres : String) =
        Pager(PagingConfig(pageSize = 10)){
            MoviePagingSource(api , sortBy , withGenres)
        }
}