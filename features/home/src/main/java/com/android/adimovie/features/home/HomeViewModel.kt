package com.android.adimovie.features.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.android.adimovie.common.base.BaseViewModel
import com.android.adimovie.common.utils.ViewState
import com.android.adimovie.common.utils.setError
import com.android.adimovie.common.utils.setLoading
import com.android.adimovie.common.utils.setSuccess
import com.android.adimovie.core.AppDispatchers
import com.android.adimovie.core.model.category.CategoryResponse
import com.android.adimovie.core.model.movie.MovieDetailResponse
import com.android.adimovie.core.model.video.VideoResponse
import com.android.adimovie.features.home.api.HomeApi
import com.android.adimovie.features.home.remote.MovieDataSource
import com.android.adimovie.features.home.remote.ReviewDataSource
import com.android.adimovie.common.extension.errorMessage
import kotlinx.coroutines.launch

class HomeViewModel ( val api : HomeApi , val dispatcher : AppDispatchers) : BaseViewModel() {
    val categoryResponse = MutableLiveData<ViewState<CategoryResponse>>()
    val movieDetailResponse = MutableLiveData<ViewState<MovieDetailResponse>>()
    val videoResponse = MutableLiveData<ViewState<VideoResponse>>()

    fun getMovieCategories() = viewModelScope.launch {
        runCatching {
            categoryResponse.setLoading()
            api.getMovieCategories()
        }.onSuccess {
            categoryResponse.setSuccess(it)
        }.onFailure {
            categoryResponse.setError(it.errorMessage())
        }
    }
    fun getMovieByCategory(sortBy : String , withGenres : String) =
        MovieDataSource(api).getMovieByCategory(sortBy , withGenres).flow.cachedIn(viewModelScope).asLiveData()

    fun getReviews(movieId : String) =
        ReviewDataSource(api).getReviews(movieId).flow.cachedIn(viewModelScope).asLiveData()

    fun getMovieDetail(movieId : String) = viewModelScope.launch {
        runCatching {
            movieDetailResponse.setLoading()
            api.getMovieDetail(movieId)
        }.onSuccess {
            movieDetailResponse.setSuccess(it)
        }.onFailure {
            movieDetailResponse.setError(it.errorMessage())
        }
    }
    fun getVideos(movieId : String) = viewModelScope.launch {
        runCatching {
            videoResponse.setLoading()
            api.getVideos(movieId)
        }.onSuccess {
            videoResponse.setSuccess(it)
        }.onFailure {
            videoResponse.setError(it.errorMessage())
        }
    }
}