package com.android.adimovie.features.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.android.adimovie.common.Constants
import com.android.adimovie.common.base.BaseViewHolder
import com.android.adimovie.common.extension.formatDate
import com.android.adimovie.core.BuildConfig
import com.android.adimovie.core.model.movie.Movie
import com.android.adimovie.features.home.databinding.ItemMovieBinding
import com.android.adimovie.common.extension.loadImageRounded


class MovieAdapter(
    diffUtil : MovieDiffUtil
) : PagingDataAdapter<Movie, MovieAdapter.ViewHolder>(diffUtil) {
    private var listener : MovieListener? = null
    fun setMovieListener(listener : MovieListener){
        this.listener = listener
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position) ?: return
        holder.bind(item , position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemMovieBinding.inflate(LayoutInflater.from(parent.context) , parent , false)
        return ViewHolder(binding)
    }
    inner class ViewHolder(binding : ItemMovieBinding) : BaseViewHolder<Movie, ItemMovieBinding>(binding){
        override fun bind(item: Movie, position: Int) {
            binding.apply {
                movieImage.loadImageRounded("${BuildConfig.BASE_IMAGE_URL}/${item.poster_path}")
                releaseDateText.text = item.release_date.formatDate(destFormat = Constants.DEFAULT_DATE_TIME)
                movieTitleText.text = item.title
                overviewText.text = item.overview
                parentContainer.setOnClickListener {
                    listener?.onClick(item)
                }
            }
        }
    }
    interface MovieListener {
        fun onClick(movie : Movie)
    }
}

class MovieDiffUtil : DiffUtil.ItemCallback<Movie>(){
    override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem == newItem
    }
}