package com.android.adimovie.features.home.remote

import com.google.gson.annotations.SerializedName

data class ErrorResponse(
    @SerializedName("code", alternate = ["errorCode"])
    val code: Int = 500,
    @SerializedName("message", alternate = ["errorMessage"])
    val message: String? = "Internal Server Error.",
    val type: Type = Type.GENERAL,
    val title: String = "" + code
) {
    enum class Type {
        GENERAL,
        HOTSPOT_LOGIN,
        UNRESOLVED_HOST,
        NO_INTERNET_CONNECTION,
        REQUEST_TIMEOUT,
        NO_DATA
    }
}
