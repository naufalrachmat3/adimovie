package com.android.adimovie.features.home

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.adimovie.common.Constants
import com.android.adimovie.common.base.BaseActivity
import com.android.adimovie.common.utils.ViewState
import com.android.adimovie.common.view.StateLayout
import com.android.adimovie.core.model.category.Category
import com.android.adimovie.features.home.adapter.CategoryAdapter
import com.android.adimovie.features.home.databinding.HomeMovieCategoryActivityBinding
import com.android.adimovie.navigation.Activities
import com.android.adimovie.navigation.startFeature
import com.android.adimovie.common.extension.binding

class HomeMovieCategoryActivity : BaseActivity<HomeViewModel>() {
    private val binding by binding<HomeMovieCategoryActivityBinding>()
    private val stateLayout by lazy {
        StateLayout(this)
            .wrap(binding.recyclerViewCategories)
            .showLoading()
    }
    private val categoryAdapter by lazy {
        CategoryAdapter()
    }
    override fun getViewModel() = HomeViewModel::class
    override fun observerViewModel() {
        viewModel.categoryResponse.onResult { state ->
            when(state){
                is ViewState.Loading -> stateLayout.showLoading()
                is ViewState.Success -> {
                    categoryAdapter.submitList(state.data.genres)
                    stateLayout.showContent()
                }
                is ViewState.Failed -> {
                    stateLayout.showError()
                    stateLayout.onRetry {
                        viewModel.getMovieCategories()
                    }
                }
            }
        }
    }

    override fun onViewCreated(savedInstanceState: Bundle?) {
        binding.apply {
            recyclerViewCategories.apply {
                layoutManager =
                    LinearLayoutManager(this@HomeMovieCategoryActivity, RecyclerView.VERTICAL, false)
                adapter = categoryAdapter
            }
            swipeRefresh.setOnRefreshListener {
                viewModel.getMovieCategories()
                swipeRefresh.isRefreshing = false
            }
        }
        categoryAdapter.setCategoryListener(object : CategoryAdapter.CategoryListener {
            override fun onClick(category: Category) {
                startFeature(Activities.HomeMovieActivity){
                    putExtra(Constants.GENRE_TITLE , category.name)
                    putExtra(Constants.GENRES , category.id)
                }
            }
        })
        viewModel.getMovieCategories()
    }
}