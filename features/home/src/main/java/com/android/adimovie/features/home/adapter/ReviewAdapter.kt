package com.android.adimovie.features.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.android.adimovie.common.base.BaseViewHolder
import com.android.adimovie.core.model.review.Review
import com.android.adimovie.features.home.BuildConfig
import com.android.adimovie.features.home.R
import com.android.adimovie.features.home.databinding.ItemReviewBinding
import com.android.adimovie.common.extension.loadImageRounded

class ReviewAdapter(
    diffUtil: ReviewDiffUtil
) : PagingDataAdapter<Review , ReviewAdapter.ViewHolder>(diffUtil) {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position) ?: return
        holder.bind(item , position)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemReviewBinding.inflate(LayoutInflater.from(parent.context) , parent , false)
        return ViewHolder(binding)
    }
    inner class ViewHolder(binding : ItemReviewBinding) : BaseViewHolder<Review , ItemReviewBinding>(binding){
        override fun bind(item: Review, position: Int) {
            binding.apply {
                if(!item.author_details.avatar_path.isNullOrEmpty())
                    avatarImage.loadImageRounded("${BuildConfig.BASE_IMAGE_URL}/${item.author_details.avatar_path}")
                else
                    avatarImage.setImageResource(com.android.adimovie.uiresources.R.drawable.ic_default_user)
                nameText.text = item.author_details.name
                reviewText.text = item.content
            }
        }
    }
}

class ReviewDiffUtil : DiffUtil.ItemCallback<Review>(){
    override fun areItemsTheSame(oldItem: Review, newItem: Review): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Review, newItem: Review): Boolean {
        return oldItem == newItem
    }
}