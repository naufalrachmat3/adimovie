package com.android.adimovie.features.home.di

import com.android.adimovie.core.di.provideApiService
import com.android.adimovie.features.home.HomeViewModel
import com.android.adimovie.features.home.api.HomeApi
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val featureHome = module {
    single { provideApiService<HomeApi>(get()) }
    viewModel { HomeViewModel(get() , get()) }
}